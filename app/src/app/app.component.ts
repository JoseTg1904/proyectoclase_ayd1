import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthUserService} from "./shared/services/auth-user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: []
})
export class AppComponent implements OnInit, OnDestroy{
  title = 'app';
  show = true;

  constructor(private authServices: AuthUserService) {
  }

  ngOnDestroy(): void {
  }

  ngOnInit(): void {

  }

  close() {
    this.show = false;
    setTimeout(() => this.show = true, 3000);
  }

}
