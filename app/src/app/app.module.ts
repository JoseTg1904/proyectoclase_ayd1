import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModuleModule } from './modules/material-module/material-module.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import './polyfills';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { ComponentsModule } from './components/components.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CookieService } from 'ngx-cookie-service';
import { MessagesModule } from 'primeng/messages';
import { MessageModule } from 'primeng/message';
import { ChipModule } from 'primeng/chip';
import { ToastModule } from 'primeng/toast';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AdminnavComponent } from './pages/adminnav/adminnav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { DashComponent } from './pages/dash/dash.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { ProductoComponent } from './pages/productos/producto/producto.component';
import { ListaProductosComponent } from './pages/productos/lista-productos/lista-productos.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { ProductoService } from './shared/services/producto.service';
import { ProductCatalogComponent } from './pages/product-catalog/product-catalog.component';
import { CartComponent } from './pages/cart/cart.component';
import { FacturacionComponent } from './pages/facturacion/facturacion.component';
import { OrdenComponent } from './pages/orden/orden.component';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule} from "ng2-charts";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    ProductDetailComponent,
    ProfileComponent,
    AdminnavComponent,
    DashComponent,
    ProductosComponent,
    ListaProductosComponent,
    ProductoComponent,
    ProductCatalogComponent,
    CartComponent,
    FacturacionComponent,
    OrdenComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', {
      enabled: environment.production,
    }),
    ComponentsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModuleModule,
    BrowserAnimationsModule,
    NgbModule,
    MessageModule,
    MessagesModule,
    ChipModule,
    ToastModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    HttpClientModule,
    MatStepperModule,
    ChartsModule
  ],
  providers: [CookieService, ProductoService],
  bootstrap: [AppComponent],
})
export class AppModule {}
