import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Cart } from 'src/app/shared/models/cart';

import { CartComponent } from './cart.component';

describe('CartComponent', () => {
  let component: CartComponent;
  let fixture: ComponentFixture<CartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [CartComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('listCart should be definded', () => {
    expect(component.listCart).toBeDefined();
  });

  describe('cartsEmpty()', () => {
    it('should be return a value boolean valid', () => {
      component.cartsEmpty();
      expect(component.cartsEmpty).toBeTrue();
      expect(component.cartsEmpty).toBeFalse();
    });
  });

  describe('modifyCart()', () => {
    it('listCart should be a defined', () => {
      component.modifyCart();
      expect(component.listCart).toBeDefined();
    });
  });
  describe('remove() should be receive a valid object Cart and index', () => {
    const cart: Cart = {};
    component.remove(cart, 0);
    expect(component.remove.arguments[0]).toBeDefined;
    expect(component.remove.arguments[1]).toBeDefined;
  });
  describe('decrement() should be receive a valid object Cart and index', () => {
    const cart: Cart = {};
    component.decrement(cart, 0);
    expect(component.decrement.arguments[0]).toBeDefined;
    expect(component.decrement.arguments[1]).toBeDefined;
  });
  describe('increment() should be receive a valid object Cart and indes', () => {
    const cart: Cart = {};
    component.increment(cart, 0);
    expect(component.increment.arguments[0]).toBeDefined;
    expect(component.increment.arguments[1]).toBeDefined;
  });
  describe('buy() should be to redirect a valid buy route', () => {
    expect(component.buy).toBeTruthy();
  });
});
