import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Cart } from 'src/app/shared/models/cart';
import { ProductoService } from 'src/app/shared/services/producto.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css'],
})
export class CartComponent implements OnInit {
  listCart: Cart[];

  total: any;

  constructor(
    private productService: ProductoService,
    private router: Router
  ) {
    this.total = 0;
  }

  ngOnInit(): void {
    this.listCart = JSON.parse(localStorage.getItem('cart'));
    this.total = this.listCart.reduce((sum, p)=> sum + (p.quantity * p.price), 0).toFixed(2)
  }

  cartsEmpty(): boolean {
    return this.listCart.length == 0;
  }

  modifyCart(): void {
    localStorage.setItem('cart', JSON.stringify(this.listCart));
  }

  remove(cart: Cart, i: number): void {
    this.listCart.splice(i, 1);
    this.modifyCart();
  }

  decrement(cart: Cart, i:number): void {
    cart.quantity = cart.quantity > 0 ? cart.quantity- 1 : cart.quantity;
    this.listCart.splice(i, 1, cart);
    this.modifyCart();
  }

  increment(cart: Cart, i: number): void {
    cart.quantity++;
    this.listCart.splice(i, 1, cart);
    this.modifyCart();
  }

  buy(): void {
    this.router.navigateByUrl('Orden');
  }
}
