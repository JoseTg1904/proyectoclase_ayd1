import {Component, OnDestroy, OnInit} from '@angular/core';
import {MatChipsModule} from '@angular/material/chips';
import {ActivatedRoute, Router} from "@angular/router";
import {FirebaseProductServiceService} from "../../shared/services/firebase-product-service.service";
import {Products} from "../../class/products";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {Product} from "../../shared/models/product";
import {Cart} from "../../shared/models/cart";
import {MatSnackBar} from "@angular/material/snack-bar";



@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.css']
})
export class ProductDetailComponent implements OnInit, OnDestroy {

  cantidad = 0;
  disponibilidad = 'disponible';
  name: any;
  private id: any;
  // @ts-ignore
  model: Products;
  //producto-info
  detalles: any;
  precio:any;
  nombre:any;
  picture:any;
  descripcion: any;
  closeResult: string;
  cantidades: number[];
  cantidadCompra: number;

  constructor(private firebaseService: FirebaseProductServiceService,
              private router: Router,
              private route: ActivatedRoute,
              private modalService: NgbModal,
              private _snackBar: MatSnackBar) {
    this.cantidadCompra = 0;

  }

  ngOnDestroy(): void {
        this.name.unsubscribe();
    }

  ngOnInit(): void {

    this.name = this.route.params.subscribe(params => {
      this.id = params['id'];
      this.cantidades = [];
      // In a real app: dispatch action to load the details here.
      this.firebaseService.readProduct(this.id).subscribe(res => {
        // @ts-ignore
        if(res.payload.data() != undefined)
        {
          this.model = new Products(res.payload.data()['name'],res.payload.data()['descripcion'],res.payload.data()['picture'],res.payload.data()['precio'],res.payload.data()['categoria'],res.payload.data()['detalles'], res.payload.data()['inventario']);
          this.cantidad = this.model.inventario;
          this.detalles = this.model.detalles;
          this.nombre = this.model.nombre;
          this.descripcion = this.model.decripcion;
          this.precio = this.model.precio;
          this.picture = this.model.picture;
          if(this.cantidad>0) this.disponibilidad = 'Disponible'
          else this.disponibilidad = 'No Disponible'
          for (let i = 1; i < this.cantidad+1; i++) {
            this.cantidades.push(i);
          }
        }
        else {
          this.router.navigate(['']);
        }

      });
    });

  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  addToCart(): void {
    var __v = JSON.parse(localStorage.getItem('cart'));
    var __k = false;
    __v.forEach( values =>{
      if(values.id == this.id)
      {
        if(this.cantidad - values.quantity > 0)
        {
          values.quantity = values.quantity+this.cantidadCompra;
          __k = true;
        }
        else {
          this.openSnackBar('Ya no puedes agregar más productos de este producto!!!');
          __k = true;
        }
      }
    });

    if(!__k)
    {
      let cart: Cart = {
        id: this.id,
        quantity: this.cantidadCompra,
        name: this.nombre,
        price: this.precio,
        photo: this.picture,
      };
      __v.push(cart);
    }
    localStorage.setItem('cart', JSON.stringify(__v));
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Aceptar', {
      duration: 1500,
    });
  }


  add(select) {
    this.cantidadCompra = select.value;
    console.log(this.cantidadCompra);
  }
}
