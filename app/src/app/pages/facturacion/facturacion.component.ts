import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import html2canvas from "html2canvas";
import pdfMake from "pdfmake/build/pdfmake";
import pdfFonts from "pdfmake/build/vfs_fonts";
import {FacturacionServiceService} from "../../shared/services/facturacion-service.service";
import {Router} from "@angular/router";
import {FirebaseUserServiceService} from "../../shared/services/firebase-user-service.service";
import {ProductoService} from "../../shared/services/producto.service";
import {AuthUserService} from "../../shared/services/auth-user.service";
import {MatSnackBar} from "@angular/material/snack-bar";
pdfMake.vfs = pdfFonts.pdfMake.vfs;


class Product{
  nombre: string;
  precio: number;
  cantidad: number;
  codigo: string;

}
class Invoice{
  no_orden: string;
  cliente: string;
  direccion: string;
  telefono: string;
  nit: string;
  additionalDetails: string;
  fecha: string;
  status:string;

  products: Product[] = [];

  constructor(){
    // Initially one empty product row we will show
    this.products.push(new Product());
  }
}

@Component({
  selector: 'app-facturacion',
  templateUrl: './facturacion.component.html',
  styleUrls: ['./facturacion.component.css']
})


export class FacturacionComponent implements OnInit {

  invoice: Invoice;
  listaFacturas: Invoice[];
  private nit:string;
  private sessionUser: string;
  private nombre: string;
  private correo: string;
  private direccion: string;
  private telefono: string;

  constructor(
      private ordenesservice: FacturacionServiceService,
      private userService: FirebaseUserServiceService,
      private productService: ProductoService,
      private autService: AuthUserService,
      private _snackBar: MatSnackBar
  ) {

    this.invoice = new Invoice();
    this.listaFacturas = [];

      this.sessionUser = (this.autService.get(sessionStorage.getItem('User')));
      this.userService.getUser(this.sessionUser).subscribe((snapshot) => {
        this.nit = snapshot['nit'];
        this.nombre = snapshot['nombre'] + snapshot['apellido'];
        this.correo = snapshot['correo'];
        this.direccion = snapshot['direccion'][0];
        this.telefono = snapshot['telefono'];
      });
  }

  ngOnInit(): void {
    this.listaFacturas = [];
    this.ordenesservice.GetOrden(this.sessionUser).subscribe(
      (r)=>
      {
        r.forEach(invoic=>
        {
          let invoiceTemp = new Invoice();
          invoiceTemp.nit = this.nit;
          invoiceTemp.cliente = this.nombre;
          invoiceTemp.direccion = this.direccion;
          invoiceTemp.telefono = this.telefono;
          // @ts-ignore
          invoiceTemp.no_orden = invoic.no_orden;
          // @ts-ignore
          invoiceTemp.fecha = invoic.fecha;
          // @ts-ignore
          invoiceTemp.status = invoic.status;

          // @ts-ignore
          let temp = invoic.metodo_pago;

          invoiceTemp.additionalDetails = temp[0];
          invoiceTemp.additionalDetails += (temp[0]=='En Efectivo')?'':temp[1];

          // @ts-ignore
          let temp1:[] = invoic.productos;
          invoiceTemp.products = [];
          temp1.forEach(valor=>
          {
            let temp2 = JSON.parse(valor);
            let niProduct = new Product();
            this.productService.readProduct(temp2.producto).subscribe(res=>
            {
              niProduct.nombre = res.payload.data()['name'];
              niProduct.precio = parseInt(res.payload.data()['precio']);
            });
            niProduct.cantidad = temp2.cantidad;
            niProduct.codigo = temp2.producto;
            invoiceTemp.products.push(niProduct);
          });
          this.listaFacturas.push(invoiceTemp);
        });
      });
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Aceptar', {
      duration: 1500,
    });
  }

  generatePDF(action = 'open') {
    let docDefinition = {
      content: [
        {
          text: 'WIKI SHOP',
          fontSize: 16,
          alignment: 'center',
          color: '#002D62',
          fontFamily: 'Ubuntu'
        },
        {
          text: 'FACTURA ELECTRONICA',
          fontSize: 20,
          bold: true,
          alignment: 'center',
          decoration: 'underline',
          color: '#002D62',
          fontFamily: 'Ubuntu'
        },
        {
          text: 'Información del Cliente',
          style: 'sectionHeader'
        },
        {
          columns: [
            [
              {
                text: `Cliente: ${this.invoice.cliente}`,
                bold:true
              },
              { text: `Direccion: ${this.invoice.direccion }`},
              { text: `Nit: ${this.invoice.nit }`},
              { text: `Telefono: ${this.invoice.telefono }`}
            ],
            [
              {
                text: `Fecha: ${this.invoice.fecha}`,
                alignment: 'right'
              },
              {
                text: `Factura No : ${this.invoice.no_orden}`,
                alignment: 'right'
              }
            ]
          ]
        },
        {
          text: 'Detalle de la Orden',
          style: 'sectionHeader'
        },
        {
          table: {
            headerRows: 1,
            widths: ['*', 'auto', 'auto', 'auto', 'auto'],
            body: [
              [{text: 'Codigo', bold: true},{text:'Producto', bold: true}, {text:'Precio',bold:true}, {text: 'Cantidad',bold:true}, {text: 'Sub Total', bold: true}],
              ...this.invoice.products.map(p => ([p.codigo, p.nombre, p.precio, p.cantidad, (p.precio*p.cantidad).toFixed(2)])),
              [{text: 'Total', colSpan: 4}, {}, {}, {}, this.invoice.products.reduce((sum, p)=> sum + (p.cantidad * p.precio), 0).toFixed(2)]
            ]
          }
        },
        {
          text: 'Informacion Adicional',
          style: 'sectionHeader'
        },
        {
          ul: [
            this.invoice.additionalDetails,
            this.invoice.status
          ] ,
          margin: [0, 0 ,0, 15]
        },
        {
          columns: [
            [{ qr: `Factura No. ${this.invoice.no_orden}, Total: ${this.invoice.products.reduce((sum, p)=> sum + (p.cantidad * p.precio), 0).toFixed(2)}`, fit: '100' }],
            [{ text: 'Signature', alignment: 'right', italics: true}],
          ]
        },
        {
          text: 'Terminos y Condiciones',
          style: 'sectionHeader'
        },
        {
          ul: [
            'Universidad de San Carlos de Guatemala',
            'Este es una factura sin valor, los estudiantes y la universidad se encuentran absueltos de cualquier penuria legal',
            'Esto es una demo.',
          ],
        }
      ],
      styles: {
        sectionHeader: {
          bold: true,
          decoration: 'underline',
          fontSize: 14,
          fontFamily: 'Ubuntu',
          margin: [0, 15,0, 15]
        }
      }
    };

    if(this.invoice.status == 'Completado')
    {
      if(action==='download'){
        pdfMake.createPdf(docDefinition).download('Factura'+this.invoice.cliente+'.pdf');
      }else if(action === 'print'){
        pdfMake.createPdf(docDefinition).print();
      }else{
        pdfMake.createPdf(docDefinition).open();
      }
    }
    else {
      this.openSnackBar(`La Orden ${this.invoice.no_orden}, se encuentra en estado: ${this.invoice.status}, por lo cual no puede generarse su peticion.`)
    }
  }

  addProduct(){
    this.invoice.products.push(new Product());
  }

  ver(fact: Invoice) {

    this.invoice = fact;

  }
}
