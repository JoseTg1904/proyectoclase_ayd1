import {AfterViewInit, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, Validators} from '@angular/forms';
import {AuthUserService} from "../../shared/services/auth-user.service";
import {ToastService} from "../../shared/services/toast-service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AngularFireAuth} from "@angular/fire/auth";


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit, AfterViewInit{

  constructor(
              private router: Router,
              public authService: AuthUserService,
              public toastService: ToastService,
              private modalService: NgbModal)
  {
    if( this.authService.adminActive || this.authService.userActive) this.router.navigate(['']);
    sessionStorage.setItem('Admin', this.authService.set('false'));
    sessionStorage.setItem('User', this.authService.set('null'));
  }

  // @ts-ignore
  ngOnInit(): void {

  }

  ngAfterViewInit(): void{
    const loginBtn = document.getElementById('login');
    loginBtn.addEventListener('click', handler);
    function handler(e) {
      const signupBtn = document.getElementById('signup');
      let parent = e.target.parentNode.parentNode;
      Array.from(e.target.parentNode.parentNode.classList).find((element) => {
        if(element !== "slide-up") {
          parent.classList.add('slide-up')
        }else{
          signupBtn.classList.add('slide-up')
          parent.classList.remove('slide-up')
        }
      });
    }
  }
  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  userFormControl = new FormControl('', [
    Validators.required,
    Validators.nullValidator,
  ]);

  pwdFormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^.{6,8}$'),
    Validators.maxLength(8),
  ]);

  emailFormControl1 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl2 = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  pwdFormControl1 = new FormControl('', [
    Validators.required,
    Validators.nullValidator,
  ]);
  errorDescription: any;
  show = true;

  close() {
    this.show = false;
    setTimeout(() => this.show = true, 3000);
  }

  showSuccess(mensaje) {
    this.toastService.show(mensaje, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(dangerTpl) {
    this.toastService.show(dangerTpl, { classname: 'bg-danger text-light', delay: 15000 });
  }

  SignUp(dg)
  {
    var email = this.emailFormControl.value;
    var password = this.pwdFormControl.value;
    var username = this.userFormControl.value;
    if(this.emailFormControl.valid && this.pwdFormControl.valid && this.userFormControl.valid)
    {
      this.authService.signUp(email,password,username).then(
        (value) =>
        {
          this.showSuccess(value);
          this.authService.Logout();
          this.router.navigate(['']);
        },
        (error) => {
          this.errorDescription = '';
          this.errorDescription = error;
          this.showDanger(dg);
        }
      );
    }
  }

  open(content,dg) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.errorDescription = '';
      var email = this.emailFormControl2.value;
      if(this.emailFormControl2.valid)
      {
        this.authService.resetPassword(email).then(
          (value) =>
          {
            if(value != '') this.showSuccess(value);
          },
          (error) => {
            this.errorDescription = '';
            this.errorDescription = error;
            this.showDanger(dg);
          }
        );
      }
    }, (reason) => {
      if(reason == 'save')
      {
        this.errorDescription = '';
        var email = this.emailFormControl2.value;
        if(this.emailFormControl2.valid)
        {
            this.authService.resetPassword(email).then(
              (value) =>
              {
                if(value != '') this.showSuccess(value);
              },
              (error) => {
                this.errorDescription = '';
                this.errorDescription = error;
                this.showDanger(dg);
                this.router.navigate(['/Login']);
              }
            );
        }
      }
    });
  }

  sigingoogle(dg)
  {
    /*
    this.authService.GoogleAuth().then(
      (value) =>
      {
        if(value != '') this.showSuccess(value);
        else this.showSuccess('Bienvenido');
        this.router.navigate(['']);
      },
      (error) => {
        this.errorDescription = '';
        this.errorDescription = error;
        this.showDanger(dg);
        this.router.navigate(['/Login']);
      }
    );
    */
    this.authService.GoogleAuth2().then((r)=>{});
    this.router.navigate(['wait']);
  }

  SignIn(dg)
  {
    this.errorDescription = '';
    var email = this.emailFormControl1.value;
    var password = this.pwdFormControl1.value;
    if(this.emailFormControl1.valid && this.pwdFormControl1.valid)
    {
      this.authService.signIn(email,password).then(
        (value) =>
        {
          if(value != '') this.showSuccess(value);
          else this.showSuccess('Bienvenido');
          if(Boolean(this.authService.adminActive))
          {
            this.router.navigate(['Admin']);
          }
          else {
            this.router.navigate(['']);
          }
        },
        (error) => {
          this.errorDescription = '';
          this.errorDescription = error;
          this.showDanger(dg);
          this.router.navigate(['/Login']);
        }
      );
    }
  }
}
