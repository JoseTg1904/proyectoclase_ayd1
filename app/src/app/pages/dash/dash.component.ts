import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {map} from 'rxjs/operators';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {ProductoService} from "../../shared/services/producto.service";
import {Label} from "ng2-charts";
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import {Data} from "@angular/router";
import {OrdenCompra} from "../../shared/models/orden-compra";
import {FirebaseUserServiceService} from "../../shared/services/firebase-user-service.service";
import {Product} from "../../shared/models/product";
import {FirebaseProductServiceService} from "../../shared/services/firebase-product-service.service";


function getRandomColor() {
  var color = Math.floor(0x1000000 * Math.random()).toString(16);
  return '#' + ('000000' + color).slice(-6);
}

export interface DataCategorias {
  nombre: string;
  id: string;
  color: string;
  cantidad: number;
  mes: string;
  anio: string;
}

@Component({
  selector: 'app-dash',
  templateUrl: './dash.component.html',
  styleUrls: ['./dash.component.css']
})
export class DashComponent implements  OnInit, OnChanges{

  ordenes_completadas: number = 0;
  money_in_da_bank: number = 0;
  productos_vendidos: number = 0;
  ordenes: OrdenCompra[];
  productos_in_stock: number = 0;
  products: Product[];
  ordenes_cancelads: number = 0;
  producto_sin_inventario: string = "";
  producto_inventario: number = 0;


  constructor(private sistemaService: ProductoService,
              private userService: FirebaseUserServiceService,
              private firebaseService: FirebaseProductServiceService) {}

  ngOnInit(): void {
    this.ordenes = [];
    this.ordenes_completadas = 0;
    this.money_in_da_bank = 0;
    this.productos_vendidos = 0;
    this.productos_in_stock = 0;
    this.products = [];
    this.ordenes_cancelads = 0;
    this.producto_sin_inventario = "";
    this.producto_inventario = 0;
    this.sistemaService.obtenerOrdenes()
      .subscribe(
        (data)=>
        {
          data.forEach(dats=>
          {
            let datito = dats.payload.doc.data();
            let orden: OrdenCompra = {
              No_Orden: datito["no_orden"],
              status: datito["status"],
              Fecha: datito["fecha"],
              Total: datito["total"],
              Productos: datito["productos"].length,
              cliente: "",
              metodo: datito["metodo_pago"][0]
            }
            this.userService.getUser(datito["cliente"]).subscribe((snapshot) => {
              orden.cliente = snapshot['nombre'] + snapshot['apellido'];
            });
            this.ordenes.push(orden);
          })
          this.ordenes.forEach(fore=>
          {
            this.ordenes_completadas += (fore.status!='Cancelado'?1:0);
            this.money_in_da_bank += (fore.status!='Cancelado'?fore.Total:0)
            this.productos_vendidos += (fore.status!='Cancelado'?fore.Productos:0)
            this.ordenes_cancelads += (fore.status=='Cancelado'?1:0)
          })

        }
      )

    this.firebaseService.readProducts().subscribe(
      (response) => {
        try {
          response.forEach((item) => {
            let data = item.payload.doc.data();
            let id_category = data['categoria'];
            let product: Product = {
              id: item.payload.doc.id,
              categoria: id_category,
              descripcion: data['descripcion'],
              detalles: data['detalles'],
              inventario: data['inventario'],
              nombre: data['name'],
              picture: data['picture'],
              precio: data['precio'],
            };
            this.products.push(product);
          });
          this.productos_in_stock = this.products.reduce((sum, p)=> sum + (parseInt(String(p.inventario))), 0)
          this.products.forEach(da=>
          {
            if(da.inventario <= this.producto_inventario)
            {
              this.producto_sin_inventario = da.nombre;
              this.producto_inventario = da.inventario;
            }
          })
        } catch (error) {
          console.log(error);
        }
      },
      (error) => console.log(error)
    );
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.ordenes = [];
    this.ordenes_completadas = 0;
    this.money_in_da_bank = 0;
    this.productos_vendidos = 0;
    this.productos_in_stock = 0;
    this.products = [];
    this.ordenes_cancelads = 0;
    this.producto_sin_inventario = "";
    this.producto_inventario = 0;
    this.sistemaService.obtenerOrdenes()
      .subscribe(
        (data)=>
        {
          data.forEach(dats=>
          {
            let datito = dats.payload.doc.data();
            let orden: OrdenCompra = {
              No_Orden: datito["no_orden"],
              status: datito["status"],
              Fecha: datito["fecha"],
              Total: datito["total"],
              Productos: datito["productos"].length,
              cliente: "",
              metodo: datito["metodo_pago"][0]
            }
            this.userService.getUser(datito["cliente"]).subscribe((snapshot) => {
              orden.cliente = snapshot['nombre'] + snapshot['apellido'];
            });
            this.ordenes.push(orden);
          })
          this.ordenes.forEach(fore=>
          {
            this.ordenes_completadas += (fore.status!='Cancelado'?1:0);
            this.money_in_da_bank += (fore.status!='Cancelado'?fore.Total:0)
            this.productos_vendidos += (fore.status!='Cancelado'?fore.Productos:0)
            this.ordenes_cancelads += (fore.status=='Cancelado'?1:0)
          })

        }
      )

    this.firebaseService.readProducts().subscribe(
      (response) => {
        try {
          response.forEach((item) => {
            let data = item.payload.doc.data();
            let id_category = data['categoria'];
            let product: Product = {
              id: item.payload.doc.id,
              categoria: id_category,
              descripcion: data['descripcion'],
              detalles: data['detalles'],
              inventario: data['inventario'],
              nombre: data['name'],
              picture: data['picture'],
              precio: data['precio'],
            };
            this.products.push(product);
          });
          this.productos_in_stock = this.products.reduce((sum, p)=> sum + (parseInt(String(p.inventario))), 0)
          this.products.forEach(da=>
          {
            if(da.inventario <= this.producto_inventario)
            {
              this.producto_sin_inventario = da.nombre;
              this.producto_inventario = da.inventario;
            }
          })
        } catch (error) {
          console.log(error);
        }
      },
      (error) => console.log(error)
    );
  }

}
