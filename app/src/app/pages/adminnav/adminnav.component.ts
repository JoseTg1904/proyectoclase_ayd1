import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { Router } from '@angular/router';
import { menuDrawer } from '../../shared/models/menuDrawer';
import { ToastService } from '../../shared/services/toast-service';

@Component({
  selector: 'app-adminnav',
  templateUrl: './adminnav.component.html',
  styleUrls: ['./adminnav.component.css'],
})
export class AdminnavComponent implements OnInit, OnDestroy {
  menuList: menuDrawer[] = [
    { value: 'Inicio', path: '/Admin/Dashboard', route: false },
    { value: 'Productos', path: '/Admin/Products', route: false }
  ];

  toggleDrawer: boolean = false;
  errorDescription: string;
  user: string;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router,
    public toastService: ToastService
  ) {
    // this.user = JSON.parse(sessionStorage.getItem('UserInfo')).User;
  }

  ngOnDestroy(): void {
    // sessionStorage.setItem('User', null);
    // sessionStorage.setItem('Admin', String(false));
  }

  ngOnInit(): void {}

  showSuccess(mensaje) {
    this.toastService.show(mensaje, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }

  showDanger(dangerTpl) {
    this.toastService.show(dangerTpl, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  toggleListen(): void {
    this.toggleDrawer = !this.toggleDrawer;
  }

  drawerListen(path: string): void {
    this.router.navigate([path]);
    this.toggleListen();
  }
}
