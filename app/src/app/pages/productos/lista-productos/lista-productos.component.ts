import { Component, OnInit } from '@angular/core';
import { elementAt } from 'rxjs/operators';
import { Producto } from 'src/app/shared/models/producto';
import { ProductoService } from 'src/app/shared/services/producto.service';
import {Categoria, Product} from "../../../shared/models/product";

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.css']
})
export class ListaProductosComponent implements OnInit {


  listaProductos = [];
  constructor(public productoService:ProductoService) { }

  ngOnInit(): void {
    /*this.productoService.getProduct()
    .snapshotChanges()
    .subscribe(item =>{
      this.listaProductos=[]
      item.forEach(element =>{
        let x=element.payload.toJSON();
        x["$key"]=element.key
        this.listaProductos.push(x as Producto)
      })
    });*/
    this.productoService.readProducts().subscribe(
      (item) =>
      {
          try {
            item.forEach( element =>
              {
                this.listaProductos=[]
                item.forEach(element =>{
                  let data = element.payload.doc.data();

                  let product: Producto = {
                    $key: element.payload.doc.id,
                    id: element.payload.doc.id,
                    descripcion: data["descripcion"],
                    detalles: data["detalles"],
                    inventario: data["inventario"],
                    nombre: data["name"],
                    picture: data["picture"],
                    precio: data["precio"]
                  }
                  let cato = data["categoria"].toString().split("/");
                  let cato1 = (cato.length>2)?cato[2]:'';
                  if (cato1!='') this.productoService.findCategorybiId(cato1).subscribe(
                    (item)=>
                    {
                      try {
                        // @ts-ignore
                        product.categoria  = item["nombre"];
                      }catch (e) {
                      }
                    }
                  );
                  this.listaProductos.push(product);
                })
              }
            );
          } catch (e) {
          }

      }
    );
  }
  editar(producto:Producto){
    this.productoService.productoSeleccionado=Object.assign({},producto)
    this.productoService.productoSeleccionado.detalles = producto.detalles;

  }
  eliminar($key:string){
    //this.productoService.deleteProduct($key)
    this.productoService.deleteProductCF($key);
  }

}
