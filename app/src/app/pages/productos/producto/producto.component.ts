import { Component, OnInit } from '@angular/core';
import {ProductoService} from '../../../shared/services/producto.service'
import {FormControl, FormGroup, NgForm, Validators} from '@angular/forms';
import { Producto } from 'src/app/shared/models/producto';
import {Categoria} from "../../../shared/models/product";
import {MatChipInputEvent} from "@angular/material/chips";
import {COMMA, ENTER} from "@angular/cdk/keycodes";
import {FirebaseProductServiceService} from "../../../shared/services/firebase-product-service.service";
import {ToastService} from "../../../shared/services/toast-service";
@Component({
  selector: 'app-producto',
  templateUrl: './producto.component.html',
  styleUrls: ['./producto.component.css']
})
export class ProductoComponent implements OnInit {

  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  archivoForm = new FormGroup({
    archivo: new FormControl(null, Validators.required),
  });

  mensajeArchivo = 'No hay un archivo seleccionado';
  datosFormulario = new FormData();
  nombreArchivo = '';
  URLPublica = '';
  porcentaje = 0;
  finalizado = false;
  readonly separatorKeysCodes: number[] = [ENTER, COMMA];
  submit = false;
  update = false;


  categorias = [];
  constructor(public productoService:ProductoService,
              public products: FirebaseProductServiceService,
              public toastService: ToastService) {}

  ngOnInit(): void {
    this.productoService.obtenerCategorias().subscribe(
      (categoria) =>
      {
        try
        {
          categoria.forEach(value=>
          {
            // @ts-ignore
            let data = value.payload.doc.data();
            let product: Categoria = {
              id: '/categoria/'+value.payload.doc.id,
              nombre: data["nombre"]
            };
            this.categorias.push(product);
          });
        }catch (e) {
        }
      }
    );
    //this.productoService.getProduct()
    this.resetear()

    if(this.finalizado && this.submit)
    {
      let referencia = this.products.referenciaCloudStorageProducts(this.nombreArchivo);
      referencia.getDownloadURL().subscribe((URL) => {
        this.URLPublica = URL;
        console.log(URL);
      });
    }
  }
  onSubmit(productForm:NgForm){
    if(productForm.value.$key!=null){
      //this.productoService.updateProduct(productForm.value)
      this.update = true;
      if(this.URLPublica != this.productoService.productoSeleccionado.picture && this.URLPublica != '') this.subirArchivo(productForm);
      if(!this.submit) this.productoService.updateProductCF(productForm.value, this.productoService.productoSeleccionado.detalles,productForm.value.$key);
      this.resetear(productForm)
    }else{
      this.subirArchivo(productForm);
      //this.productoService.insertProduct(productForm.value)
    }
  }

  resetear(productForm?:NgForm){
    if(productForm!=null){
      productForm.reset();
      this.productoService.productoSeleccionado= new Producto()
      this.productoService.productoSeleccionado.detalles = [];
      this.porcentaje = 0;
      this.finalizado = false;
      this.submit = false;
      this.datosFormulario.delete('archivo');
    }
  }

  add(event: MatChipInputEvent): void {
    if(this.productoService.productoSeleccionado.detalles == undefined) this.productoService.productoSeleccionado.detalles = [];
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      // @ts-ignore
      this.productoService.productoSeleccionado.detalles.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }
  }

  remove(detail): void {
    let indext = -1;
    let find = false;
    this.productoService.productoSeleccionado.detalles.forEach(value => {
      if(!find) indext++;
      if(value == detail) find = true;
    });

    if (indext >= 0 && find) {
      this.productoService.productoSeleccionado.detalles.splice(indext, 1);
    }
  }

  cambioArchivo(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.mensajeArchivo = `Archivo preparado: ${event.target.files[i].name}`;
        this.nombreArchivo = event.target.files[i].name;
        this.datosFormulario.delete('archivo');
        this.datosFormulario.append('archivo', event.target.files[i], event.target.files[i].name)
      }
    } else {
      this.mensajeArchivo = 'No hay un archivo seleccionado';
    }
  }

  subirArchivo(productForm:NgForm) {
    let archivo = this.datosFormulario.get('archivo');
    let tarea = this.products.tareaCloudStorageProducts(this.nombreArchivo, archivo);


    //Cambia el porcentaje
    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      if (this.porcentaje == 100) {
        this.finalizado = true;
        this.submit = true;
        let referencia = this.products.referenciaCloudStorageProducts(this.nombreArchivo);
        referencia.getDownloadURL().subscribe((URL) => {
          this.URLPublica = URL;
          productForm.value.picture = this.URLPublica;
          if(!this.update) this.productoService.insertProductCF(productForm.value, this.productoService.productoSeleccionado.detalles);
          else {
            this.productoService.updateProductCF(productForm.value, this.productoService.productoSeleccionado.detalles,productForm.value.$key);
          }
          this.resetear(productForm)
        });
      }
    });
  }

  showSuccess(mensaje) {
    this.toastService.show(mensaje, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }

  showDanger(dangerTpl) {
    this.toastService.show(dangerTpl, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

}
