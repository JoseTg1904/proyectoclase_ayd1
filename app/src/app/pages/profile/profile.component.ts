import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Profile } from '../../class/user';
import { AuthUserService } from '../../shared/services/auth-user.service';
import { FirebaseUserServiceService } from '../../shared/services/firebase-user-service.service';
import { formatDate } from '@angular/common';
import { FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { FirebaseProductServiceService } from '../../shared/services/firebase-product-service.service';
import { ToastService } from '../../shared/services/toast-service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
})
export class ProfileComponent implements OnInit {
  model: Profile;
  disabled: boolean = true;
  sessionUser: any;

  archivoForm = new FormGroup({
    archivo: new FormControl(null, Validators.required),
  });

  mensajeArchivo = 'No hay un archivo seleccionado';
  datosFormulario = new FormData();
  nombreArchivo = '';
  URLPublica = '';
  porcentaje = 0;
  finalizado = false;

  constructor(
    public authServices: AuthUserService,
    private userService: FirebaseUserServiceService,
    private router: Router,
    public products: FirebaseProductServiceService,
    public toastService: ToastService
  ) {
    this.model = new Profile(0, '', '', '', [], 0, '', '', 0, '', '', '', '');
    this.sessionUser = this.authServices.get(sessionStorage.getItem('User'));
    this.porcentaje = 0;
    this.finalizado = false;
  }

  ngOnInit(): void {
    this.userService.getUser(this.sessionUser).subscribe((snapshot) => {
      this.model.edad = snapshot['edad'];
      this.model.nit = snapshot['nit'];
      this.model.apellido = snapshot['apellido'];
      this.model.correo = snapshot['correo'];
      this.model.direccion = snapshot['direccion'];
      this.model.dpi = snapshot['dpi'];
      this.model.nombre = snapshot['nombre'];
      this.model.profile = snapshot['profile'];
      this.model.telefono = snapshot['telefono'];
      this.model.usuario = snapshot['usuario'];
      this.model.creacion = snapshot['creacion'];
      this.model.modificacion = snapshot['modificacion'];
      this.model.picture = snapshot['picture'];
    });
  }

  updateUser() {
    // @ts-ignore
    this.model.modificacion = String(new Date());
    this.disabled = true;
    this.userService.updateUser(this.sessionUser, {
      edad: this.model.edad,
      nit: this.model.nit,
      apellido: this.model.apellido,
      correo: this.model.correo,
      direccion: this.model.direccion,
      dpi: this.model.dpi,
      nombre: this.model.nombre,
      profile: this.model.profile,
      telefono: this.model.telefono,
      usuario: this.model.usuario,
      creacion: this.model.creacion,
      modificacion: this.model.modificacion,
      picture: this.model.picture,
    });
    this.finalizado = false;
    this.porcentaje = 0;
    this.datosFormulario.delete('archivo');
  }

  deleteUser() {
    this.authServices.deleteuser();
    this.router.navigate(['/home']);
  }

  onSubmit(productForm: NgForm) {
    this.subirArchivo(productForm);
  }

  cambioArchivo(event) {
    if (event.target.files.length > 0) {
      for (let i = 0; i < event.target.files.length; i++) {
        this.mensajeArchivo = `Archivo preparado: ${event.target.files[i].name}`;
        this.nombreArchivo = event.target.files[i].name;
        this.datosFormulario.delete('archivo');
        this.datosFormulario.append(
          'archivo',
          event.target.files[i],
          event.target.files[i].name
        );
      }
    } else {
      this.mensajeArchivo = 'No hay un archivo seleccionado';
    }
  }

  subirArchivo(productForm: NgForm) {
    let archivo = this.datosFormulario.get('archivo');
    let tarea = this.products.tareaCloudStorageUsers(
      this.nombreArchivo,
      archivo
    );

    //Cambia el porcentaje
    tarea.percentageChanges().subscribe((porcentaje) => {
      this.porcentaje = Math.round(porcentaje);
      if (this.porcentaje == 100) {
        this.finalizado = true;
        let referencia = this.products.referenciaCloudStorageUser(
          this.nombreArchivo
        );
        referencia.getDownloadURL().subscribe((URL) => {
          this.model.picture = URL;
          this.updateUser();
        });
      }
    });
  }
}
