import {
  AfterViewInit,
  Component,
  ElementRef,
  NgZone,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Cart } from 'src/app/shared/models/cart';
import { MatStepper } from '@angular/material/stepper';
import { FirebaseUserServiceService } from '../../shared/services/firebase-user-service.service';
import { Profile } from '../../class/user';
import { AuthUserService } from '../../shared/services/auth-user.service';
import { FacturacionServiceService } from '../../shared/services/facturacion-service.service';

interface Pago {
  value: number;
  viewValue: string;
}

class Invoice {
  no_orden: string;
  cliente: string;
  direccion: string;
  telefono: string;
  nit: string;
  additionalDetails: string;
  fecha: string;
  status: string;

  constructor() {
    // Initially one empty product row we will show
  }
}

/**
 * @title Stepper overview
 */
@Component({
  selector: 'app-orden',
  templateUrl: './orden.component.html',
  styleUrls: ['./orden.component.css'],
})
export class OrdenComponent implements OnInit, AfterViewInit {
  @ViewChild('stepper') private myStepper: MatStepper;
  @ViewChild('cardInfo') private cardInfo: ElementRef;
  cardError: string;
  card: any;

  isLinear = true;
  invoice: Invoice;

  listCart: Cart[];
  cantidad: any;
  total: number;

  direccion: any;
  nit: any;
  cvv: any;

  seleccion: number = 0;
  #sessionUser: string;
  no_orden: number;
  private idUser: any;
  payment: number = 2;
  id: string;
  model: Profile;
  tarjeta: string;

  constructor(
    private router: Router,
    private userService: FirebaseUserServiceService,
    public authServices: AuthUserService,
    private facturacionService: FacturacionServiceService,
    private ngZone: NgZone
  ) {
    this.invoice = new Invoice();
    this.tarjeta = '';
    this.model = new Profile(0, '', '', '', [], 0, '', '', 0, '', '', '', '');
    this.#sessionUser = this.authServices.get(sessionStorage.getItem('User'));
    this.facturacionService.GetNoOrden().subscribe((result) => {
      this.no_orden = result.length + 1;
    });
    this.userService.getUser(this.#sessionUser).subscribe((snapshot) => {
      this.model.nit = snapshot['nit'];
      this.model.direccion = snapshot['direccion'];
      this.model.nombre = snapshot['nombre'] + snapshot['apellido'];
      this.model.telefono = snapshot['telefono'];
    });
  }

  ngOnInit() {
    this.listCart = JSON.parse(localStorage.getItem('cart'));
    this.total = Number(
      this.listCart.reduce((sum, p) => sum + p.quantity * p.price, 0)
    );
  }

  ngAfterViewInit() {
    this.card = elements.create('card');
    this.card.mount(this.cardInfo.nativeElement);
    this.card.addEventListener('change', this.onChange.bind(this));
  }

  onChange({ error }) {
    if (error) this.ngZone.run(() => (this.cardError = error.message));
    else this.ngZone.run(() => (this.cardError = null));
  }

  async pagar() {
    const { token, error } = await stripe.createToken(this.card);
    if (token) {
      const response = await this.facturacionService.cargo(
        this.total,
        token.id
      );
      console.log(response);
      this.Generar();
    } else this.ngZone.run(() => (this.cardError = error.message));
  }

  carrito(): void {
    this.router.navigateByUrl('Carrito');
  }

  changePays(pays) {
    this.payment = pays.value;
    console.log(this.payment);
  }

  verFacturas() {
    this.router.navigate(['home', 'Facturacion']);
  }

  Generar() {
    let productos = [];
    this.listCart.forEach((products) => {
      let producto = {
        producto: products.id,
        cantidad: products.quantity,
      };
      // @ts-ignore
      productos.push(JSON.stringify(producto));
    });
    let orden = {
      no_orden: this.no_orden,
      cliente: this.#sessionUser,
      status: 'Procesandose',
      productos: productos,
      fecha: String(new Date()),
      metodo_pago: [this.payment, this.tarjeta],
      total: this.total,
    };
    this.facturacionService.GenerarOrden(orden);
  }
}
