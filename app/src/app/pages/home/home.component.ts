import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Cart } from 'src/app/shared/models/cart';
import { menuDrawer } from 'src/app/shared/models/menuDrawer';
import { ToastService } from '../../shared/services/toast-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit, OnDestroy {
  menuList: menuDrawer[] = [
    { value: 'Inicio', path: 'Products', route: true },
    { value: 'Perfil', path: 'Profile', route: true },
    { value: 'Productos', path: 'Products', route: true },
    { value: 'Carrito', path: 'Carrito', route: true },
    { value: 'Facturas', path: 'Facturacion', route: true},
    { value: 'Iniciar sesión', path: 'Login', route: false },
  ];

  listCart: Cart[] = [];

  toggleDrawer: boolean = false;
  errorDescription: string;

  constructor(
    public toastService: ToastService,
    public router: Router,
    public route: ActivatedRoute
  ) {
    this.errorDescription = '';
  }

  ngOnInit(): void {
    localStorage.setItem('cart', JSON.stringify(this.listCart));
  }

  showSuccess(mensaje) {
    this.toastService.show(mensaje, {
      classname: 'bg-success text-light',
      delay: 5000,
    });
  }

  showDanger(dangerTpl) {
    this.toastService.show(dangerTpl, {
      classname: 'bg-danger text-light',
      delay: 5000,
    });
  }

  toggleListen(): void {
    this.toggleDrawer = !this.toggleDrawer;
  }

  drawerListen(path: string): void {
    //console.log(path);
    if(path === 'Login') this.router.navigate(['Login'])
    else
    {
      this.router.navigate(['home', path]);
      this.toggleListen();
    }
  }

  ngOnDestroy(): void {
  }
}
