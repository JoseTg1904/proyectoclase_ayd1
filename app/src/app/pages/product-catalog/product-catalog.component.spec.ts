import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Product } from 'src/app/shared/models/product';
import { Producto } from 'src/app/shared/models/producto';

import { ProductCatalogComponent } from './product-catalog.component';

describe('ProductCatalogComponent', () => {
  let component: ProductCatalogComponent;
  let fixture: ComponentFixture<ProductCatalogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ProductCatalogComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductCatalogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('addToCart should be recive a Product instance object', () => {
    // ARRANGE
    const product: Producto = { $key: '' };
    component.addToCart(product);
    // ASSERT
    const response = component.products;
    // ACT
    expect(response).toBeDefined();
  });

  it('addToCart should push a defined list cart', () => {
    // ARRANGE
    const product: Producto = { $key: '' };
    component.addToCart(product);
    // ASSERT
    const response = component.listCart;
    // ACT
    expect(response).toBeDefined();
  });
});
