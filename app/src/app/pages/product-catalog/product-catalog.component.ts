import { Component, OnDestroy, OnInit } from '@angular/core';
import { Cart } from '../../shared/models/cart';
import { FirebaseProductServiceService } from '../../shared/services/firebase-product-service.service';
import { Product, Categoria } from '../../shared/models/product';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-product-catalog',
  templateUrl: './product-catalog.component.html',
  styleUrls: ['./product-catalog.component.css'],
})
export class ProductCatalogComponent implements OnInit, OnDestroy {
  listCart: Cart[];
  products: Product[];
  categories: Categoria[];
  name: any;
  cat: any;
  grid: boolean = true;
  lista: boolean;
  blocks: boolean;
  hide: boolean = true;

  constructor(
    private firebaseService: FirebaseProductServiceService,
    private _snackBar: MatSnackBar
  ) {
    this.name = '';
    this.cat = '';
    this.blocks = true;
    this.lista = false;
  }

  ngOnInit(): void {
    this.getCategories();
    this.listProducts(this.cat);
    this.listCart = JSON.parse(localStorage.getItem('cart'));
  }

  addToCart(product: Product): void {
    var __v = JSON.parse(localStorage.getItem('cart'));
    var __k = false;
    __v.forEach((values) => {
      if (values.id == product.id) {
        if (product.inventario - values.quantity > 0) {
          values.quantity = values.quantity + 1;
          __k = true;
        } else {
          this.openSnackBar(
            'Ya no puedes agregar más productos de este producto!!!'
          );
          __k = true;
        }
      }
    });

    if (!__k) {
      let cart: Cart = {
        id: product.id,
        quantity: 1,
        name: product.nombre,
        price: product.precio,
        photo: product.picture,
      };
      __v.push(cart);
    }
    localStorage.setItem('cart', JSON.stringify(__v));
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Aceptar', {
      duration: 1500,
    });
  }

  listProducts(category: string): void {
    this.products = [];
    this.cat = category;
    this.firebaseService.readProducts().subscribe(
      (response) => {
        try {
          response.forEach((item) => {
            let data = item.payload.doc.data();
            let id_category = data['categoria'];
            if (
              (category == '' &&
                (data['name'].includes(this.name) || this.name == '')) ||
              (`/categoria/${category}` == id_category &&
                (data['name'].includes(this.name) || this.name == ''))
            ) {
              let product: Product = {
                id: item.payload.doc.id,
                categoria: id_category,
                descripcion: data['descripcion'],
                detalles: data['detalles'],
                inventario: data['inventario'],
                nombre: data['name'],
                picture: data['picture'],
                precio: data['precio'],
              };
              this.categories.forEach((cato) => {
                if (cato.id == data['categoria']) product.cat = cato.nombre;
              });
              this.products.push(product);
            }
          });
        } catch (error) {
          console.log(error);
        }
      },
      (error) => console.log(error)
    );
  }

  getCategories(): void {
    this.categories = [];
    this.firebaseService.readCategories().subscribe(
      (res) => {
        try {
          res.forEach((item) => {
            let doc = item.payload.doc;
            let data = doc.data();
            let category: Categoria = {
              id: doc.id,
              nombre: data['nombre'],
            };
            this.categories.push(category);
          });
        } catch (err) {
          console.log(err);
        }
      },
      (err) => console.log(err)
    );
  }

  ngOnDestroy(): void {}

  Buscar(busqueda) {
    this.name = busqueda.value;
    console.log(busqueda.value);
    this.listProducts(this.cat);
  }

  list() {
    this.lista = true;
    this.blocks = false;
    this.grid = false;
  }

  block() {
    this.blocks = true;
    this.lista = false;
    this.grid = true;
  }

  hideSearch() {
    this.hide = !this.hide;
  }
}
