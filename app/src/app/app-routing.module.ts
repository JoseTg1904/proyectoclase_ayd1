import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AdminnavComponent } from './pages/adminnav/adminnav.component';
import { DashComponent } from './pages/dash/dash.component';
import { ProductCatalogComponent } from './pages/product-catalog/product-catalog.component';
import { ProductosComponent } from './pages/productos/productos.component';
import { CartComponent } from './pages/cart/cart.component';
import { FacturacionComponent } from './pages/facturacion/facturacion.component';
import { OrdenComponent } from './pages/orden/orden.component';
import { UserGuard } from './shared/guard/user.guard';
import { AdminGuard } from './shared/guard/admin.guard';
import {WaitComponent} from "./components/wait/wait.component";

const routes: Routes = [
  { path: '', pathMatch: 'prefix', redirectTo: 'home' },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'Products' },
      { path: 'Products', component: ProductCatalogComponent },
      {
        path: 'Facturacion',
        component: FacturacionComponent,
        canActivate: [UserGuard],
      },
      {
        path: 'Profile',
        component: ProfileComponent,
        canActivate: [UserGuard],
        data: { breadcrumb: 'Profile' },
      },
      { path: 'Products', component: ProductCatalogComponent },
      {
        path: 'Products/:id',
        data: { breadcrumb: 'Detail' },
        component: ProductDetailComponent,
      },
      {
        path: 'Carrito',
        component: CartComponent,
        data: { breadcrumb: 'Carrito' },
      },
      { path: 'Orden',
        component: OrdenComponent,
        canActivate: [UserGuard],
        data: { breadcrumb: 'Orden' },
      },
    ],
  },
  {
    path: 'Login',
    data: { breadcrumb: 'login' },
    component: LoginComponent,
  },
  {
    path: 'wait',
    data: { breadcrumb: 'wait' },
    component: WaitComponent,
  },
  {
    path: 'Admin',
    component: AdminnavComponent,

    children: [
      { path: '', redirectTo: 'Dashboard', pathMatch: 'full' },
      { path: 'Dashboard', component: DashComponent },
      { path: 'Products', component: ProductosComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
