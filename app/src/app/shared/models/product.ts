export interface Product {
  id          ?: string;
  categoria   ?: string;
  descripcion ?: any;
  detalles    ?: string[];
  inventario  ?: number;
  nombre      ?: string;
  picture     ?: string;
  precio      ?: number;
  cat         ?: string;
}

export interface Categoria {
  id      ?: string;
  nombre  ?: string;
}
