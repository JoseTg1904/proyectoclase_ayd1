export interface menuDrawer {
  value: string;
  path: string;
  route: Boolean;
}
