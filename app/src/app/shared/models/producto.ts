export class Producto {
    $key:string;
    id?:string;
    categoria?: string;
    descripcion?: any;
    detalles?: [];
    inventario?: number;
    nombre?: string;
    picture?: string;
    precio?: number;
    detalles1?: [];
    URLPublica?: string;
}
