import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root',
})
export class FacturacionServiceService {
  headers = new HttpHeaders();
  constructor(
    private FirebaseService: AngularFirestore,
    private http: HttpClient
  ) {
    this.headers.set('Content-Type', 'application/json');
  }

  GetOrden(uid) {
    return this.FirebaseService.collection('orden', (ref) =>
      ref.where('cliente', '==', uid)
    ).valueChanges();
  }

  // @ts-ignore
  GetNoOrden() {
    return this.FirebaseService.collection('orden').valueChanges();
  }

  GenerarOrden(orden) {
    this.FirebaseService.collection('orden').add(orden);
  }

  cargo(cantidad, tokenId) {
    console.log({ stripeToken: tokenId, cantidad: cantidad });
    return this.http
      .post(
        'https://3v03h3pde6.execute-api.us-west-2.amazonaws.com/Primera/stripe_checkout',
        {
          stripeToken: tokenId,
          cantidad: cantidad,
        },
        { headers: this.headers }
      )
      .toPromise();
  }
}
