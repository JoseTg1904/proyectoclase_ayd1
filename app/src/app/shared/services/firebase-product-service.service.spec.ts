import { TestBed } from '@angular/core/testing';

import { FirebaseProductServiceService } from './firebase-product-service.service';

describe('FirebaseProductServiceService', () => {
  let service: FirebaseProductServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseProductServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
