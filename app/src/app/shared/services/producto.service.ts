import { Injectable } from '@angular/core';
import {
  AngularFireDatabase,
  AngularFireDatabaseModule,
  AngularFireList,
} from '@angular/fire/database';
import { Producto } from '../models/producto';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import {DataCategorias} from "../../pages/dash/dash.component";

function getRandomColor() {
  var color = Math.floor(0x1000000 * Math.random()).toString(16);
  return '#' + ('000000' + color).slice(-6);
}

@Injectable({
  providedIn: 'root',
})
export class ProductoService {
  listaproductos: AngularFireList<any>;
  productoSeleccionado: Producto = new Producto();

  constructor(
    private firebase: AngularFireDatabase,
    private FirebaseService: AngularFirestore,
    private storage: AngularFireStorage
  ) {}

  getProduct() {
    return (this.listaproductos = this.firebase.list('productos'));
  }

  insertProduct(producto: Producto) {
    this.listaproductos.push({
      nombre: producto.nombre,
      descripcion: producto.descripcion,
      picture: producto.picture,
      precio: producto.precio,
      categoria: producto.categoria,
      inventario: producto.inventario,
    });
  }

  insertProductCF(producto: Producto, detalles) {
    return this.FirebaseService.collection('catalogo').add({
      name: producto.nombre,
      descripcion: producto.descripcion,
      picture: producto.picture,
      precio: producto.precio,
      categoria: producto.categoria,
      inventario: producto.inventario,
      detalles: detalles,
    });
  }

  updateProductCF(producto: Producto, detalles, uid) {
    return this.FirebaseService.collection('catalogo').doc(uid).set({
      name: producto.nombre,
      descripcion: producto.descripcion,
      picture: producto.picture,
      precio: producto.precio,
      categoria: producto.categoria,
      inventario: producto.inventario,
      detalles: detalles,
    });
  }

  updateProduct(producto: Producto) {
    this.listaproductos.update(producto.$key, {
      name: producto.nombre,
      descripcion: producto.descripcion,
      picture: producto.picture,
      precio: producto.precio,
      categoria: producto.categoria,
      inventario: producto.inventario,
    });
  }
  deleteProduct($key: string) {
    this.listaproductos.remove($key);
  }

  deleteProductCF(uid) {
    return this.FirebaseService.collection('catalogo').doc(uid).delete();
  }
  findCategorybiId(uid) {
    return this.FirebaseService.collection('categoria').doc(uid).valueChanges();
  }

  readProduct(uid) {
    return this.FirebaseService.collection('catalogo')
      .doc(uid)
      .snapshotChanges();
  }
  readProducts() {
    return this.FirebaseService.collection('catalogo').snapshotChanges();
  }
  obtenerCategorias() {
    return this.FirebaseService.collection('categoria').snapshotChanges();
  }
  obtenerOrdenes(){
    return this.FirebaseService.collection("orden", ref => ref.orderBy("productos", "asc"))
      .snapshotChanges();
  }

  getCategoriId(idcat)
  {
    return this.FirebaseService.collection("categoria",ref => ref.where("nombre","==", idcat))
      .snapshotChanges();
  }

  getProductCategory(categori)
  {
    return this.FirebaseService.collection("catalogo",ref => ref.where("categoria","==", categori))
      .snapshotChanges();
  }

}
