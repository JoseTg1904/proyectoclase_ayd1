import { TestBed } from '@angular/core/testing';

import { FirebaseUserServiceService } from './firebase-user-service.service';

describe('FirebaseUserServiceService', () => {
  let service: FirebaseUserServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FirebaseUserServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
