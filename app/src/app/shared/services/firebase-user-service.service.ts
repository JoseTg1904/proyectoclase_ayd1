import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Profile } from '../../class/user';
import {reduce} from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class FirebaseUserServiceService {
  constructor(
    private FirebaseService: AngularFirestore
  ) {}


  deleteuser(uid)
  {
    this.FirebaseService.collection('perfil').doc(uid).delete();
  }

  findIdByemail(email)
  {
    return this.FirebaseService.collection('perfil', ref => ref.where('correo','==', email))
      .valueChanges()
      .pipe(reduce((acc, curr) => curr[0], {}));
  }

  findUser(email)
  {
    return this.FirebaseService.collection('perfil', ref => ref.where('correo','==', email))
      .valueChanges();
  }

  findUserUid(uid)
  {
    return this.FirebaseService.collection('perfil').doc(uid).valueChanges();
  }


  createuser(uid, usuario: Profile) {
    return this.FirebaseService.collection('perfil').doc(uid).set({
      edad: usuario.edad,
      nit: usuario.nit,
      apellido: usuario.apellido,
      correo: usuario.correo,
      direccion: usuario.direccion,
      dpi: usuario.dpi,
      nombre: usuario.nombre,
      profile: usuario.profile,
      telefono: usuario.telefono,
      usuario: usuario.usuario,
      modificacion: usuario.modificacion,
      creacion: usuario.creacion,
      picture: usuario.picture
    });
  }

  readUser(uid) {
    return this.FirebaseService.collection('perfil').doc(uid).snapshotChanges();
  }

  getUser(uid) {
    return this.FirebaseService.collection('perfil').doc(uid).valueChanges();
  }

  updateUser(uid, user) {
    this.FirebaseService.collection('perfil').doc(uid).set(user);
  }
}
