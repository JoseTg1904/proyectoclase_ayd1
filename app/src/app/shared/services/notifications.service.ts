import { Injectable } from '@angular/core';
import {notification} from "../../class/user";
import {Observable, Subject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  constructor() { }
  // @ts-ignore
  showNotificacionSource: Subject = new Subject();

  // @ts-ignore
  getNotificacion(): Observable {
    return this.showNotificacionSource.asObservable();
  }

  showError(msg: string, summary?: string) {
    this.show('error', summary, msg);
  }

  showSuccess(msg: string, summary?: string) {
    this.show('success', summary, msg);
  }

  showInfo(msg: string, summary?: string) {
    this.show('info', summary, msg);
  }

  showWarn(msg: string, summary?: string) {
    this.show('warn', summary, msg);
  }

  private show(severity: string, summary: string, msg: string) {
    const notificacion: notification = {
      severity: severity,
      summary: summary,
      detail: msg
    };

    this.notify(notificacion);

  }

  private notify(notificacion: notification): void {
    this.showNotificacionSource.next(notificacion);
  }
}
