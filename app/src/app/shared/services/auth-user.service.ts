import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';
import { Profile } from '../../class/user';
import { FirebaseUserServiceService } from './firebase-user-service.service';
import * as CryptoJS from 'crypto-js';
import auth = firebase.auth;
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthUserService {
  // @ts-ignore
  #secretpass = 'AyD1UsaCg2';

  constructor(
    private afAuth: AngularFireAuth,
    private userService: FirebaseUserServiceService,
    private router: Router
  ) {}

  protected AdminUser = [
    { user: 'Admin', email: 'admin@usac.com', id: 1 },
    { User: 'Jose', email: 'jose@usac.com', id: 2 },
    { User: 'Marco', email: 'marco@usac.com', id: 3 },
    { User: 'Heidy', email: 'heidy@usac.com', id: 4 },
    { User: 'Kevin', email: 'kevin@usac.com', id: 5 },
    { User: 'Javier', email: 'javier@usac.com', id: 6 },
    { User: 'Wannan', email: 'wannan@usac.com', id: 7 },
  ];
  protected AdminPass = [
    'pass123',
    'A1Admin',
    'Analisis1',
    'Usac2021Analisis1',
  ];

  user() {
    return this.afAuth.user;
  }

  Logout() {
    this.afAuth
      .signOut()
      .then((r) => console.log('Adios :c'))
      .catch((e) => console.log('Ocurrio un error.. :v'));
  }

  set(value) {
    return CryptoJS.AES.encrypt(
      value.toString().trim(),
      this.#secretpass.trim()
    ).toString();
  }

  get(value: string) {
    return CryptoJS.AES.decrypt(value.trim(), this.#secretpass.trim()).toString(
      CryptoJS.enc.Utf8
    );
  }

  LogoutAdmin() {
    sessionStorage.setItem('Admin', this.set('false'));
    sessionStorage.setItem('User', this.set('null'));
  }

  infoUser() {
    return this.afAuth.currentUser;
  }

  get userActive(): boolean {
    return firebase.auth().currentUser != null;
  }

  get adminActive(): boolean {
    let admin = sessionStorage.getItem('Admin');
    return admin == 'true';
  }

  isAdmin(email, pass) {
    for (let mail of this.AdminUser) {
      if (mail.email == email) {
        for (let pwd of this.AdminPass) {
          if (pwd == pass) return mail;
        }
      }
    }
    return null;
  }

  signUp(email, password, username) {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.afAuth
          .createUserWithEmailAndPassword(email, password)
          .then((user) => {
            // Signed in
            user.user
              .updateProfile({ displayName: username, photoURL: null })
              .then((r) => {
                // @ts-ignore
                let usuarionuevo = new Profile(
                  0,
                  '',
                  '',
                  email,
                  ['Guatemala'],
                  0,
                  '',
                  '',
                  0,
                  username,
                  String(new Date()),
                  String(new Date()),
                  ''
                );

                this.userService
                  .createuser(user.user.uid, usuarionuevo)
                  .then((r) => {
                    user.user
                      .sendEmailVerification()
                      .then((r) => {
                        //unicamente para determinar si envio el correo correctamente
                        resolve(
                          'Te hemos enviado un correo para que verifiques tu cuenta.'
                        );
                        //enviar un alert
                      })
                      .catch((error) => {
                        reject(
                          'No es posible enviarte un email de verificacion, revisa tu correo electronico.'
                        );
                      });
                  })
                  .catch((error) => {
                    reject(
                      'No es posible crear tu perfil de usuario, en estos momentos.'
                    );
                    let id = 0;
                    this.userService.findIdByemail(email).subscribe((r) => {
                      // @ts-ignore
                      id = r[0].correo;
                    });
                    this.userService.deleteuser(id);
                  });
              })
              .catch((error) => {
                reject(error.message);
              });
          })
          .catch((error) => {
            reject(error.message);
          });
      });
    });
  }

  signIn(email, password) {
    // @ts-ignore
    let error = false;
    let admin = this.isAdmin(email, password);
    if (admin != null)
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          sessionStorage.setItem('User', this.set(admin.id));
          sessionStorage.setItem('Admin', this.set('true'));
          if (error) {
            reject();
          } else {
            this.router.navigate(['Admin']);
          }
        }, 1000);
      });
    else
      return new Promise((resolve, reject) => {
        this.afAuth
          .signInWithEmailAndPassword(email, password)
          .then((r) => {
            let userIn = this.userActive;
            if (userIn) {
              this.user().subscribe((usuario) => {
                if (usuario != null) {
                  if (usuario.emailVerified) {
                    sessionStorage.setItem('User', this.set(r.user.uid));
                    //sessionStorage.setItem('UserInfo', JSON.stringify(r.user));
                    sessionStorage.setItem('Admin', this.set('false'));
                    resolve('');
                  } else {
                    reject(
                      'Usuario no verificado, por favor verifica tu correo.'
                    );
                    this.Logout();
                  }
                } else {
                  reject(
                    'Usuario no verificado, por favor verifica tu correo.'
                  );
                  this.Logout();
                }
              });
            }
          })
          .catch((error) => {
            reject(error.message);
          });
      });
  }

  resetPassword(email) {
    return new Promise((resolve, reject) => {
      this.afAuth
        .sendPasswordResetEmail(email)
        .then((r) => {
          resolve(
            'Te hemos enviado un correo con las instrucciones para recuperar tu contraseña'
          );
        })
        .catch((error) => {
          reject(error.message);
        });
    });
  }

  GoogleAuth2() {
    return this.AuthLogin2(new auth.GoogleAuthProvider())
      .then((result) => {
        console.log(result);
      })
      .catch((error) => {
        console.log(error.message);
      });
  }

  AuthLogin2(provider) {
    return this.afAuth.signInWithRedirect(provider);
  }

  ResultRedirect() {
    return new Promise((resolve, reject) => {
      this.afAuth
        .getRedirectResult()
        .then((result) => {
          if (result.credential) {
            /** @type {firebase.auth.OAuthCredential} */
            var credential = result.credential;

            // This gives you a Google Access Token. You can use it to access the Google API.
            //console.log(credential);
            // The signed-in user info.
            var user = result.user;
            this.userService.findUser(user.email).subscribe((result1) => {
              // @ts-ignore
              if (result1.length == 0) {
                this.userService
                  .createuser(
                    user.uid,
                    new Profile(
                      0,
                      '',
                      '',
                      user.email,
                      ['Guatemala'],
                      0,
                      user.displayName,
                      '',
                      0,
                      user.displayName,
                      String(new Date()),
                      String(new Date()),
                      ''
                    )
                  )
                  .then((result) => {
                    user
                      .sendEmailVerification()
                      .then((fin) => {
                        resolve(
                          'Te hemos enviado un correo para que verifiques tu cuenta.'
                        );
                      })
                      .catch((error2) => {
                        reject(error2.message);
                      });
                  })
                  .catch((error1) => {
                    reject(error1.message);
                  });
              } else {
                sessionStorage.setItem('User', this.set(user.uid));
                sessionStorage.setItem('Admin', this.set('false'));
                //sessionStorage.setItem('UserInfo', JSON.stringify(r.user));
                resolve('Bienvenido, ' + user.displayName);
              }
            });
          } else {
            resolve('noyet');
          }
        })
        .catch((error) => {
          reject(error.message);
        });
    });
  }

  GoogleAuth() {
    return new Promise((resolve, reject) => {
      this.AuthLogin(new auth.GoogleAuthProvider()).then(
        (value) => resolve(value),
        (value1) => reject(value1)
      );
    });
  }

  // Auth logic to run auth providers
  AuthLogin(provider) {
    return new Promise((resolve, reject) => {
      this.afAuth
        .signInWithPopup(provider)
        .then((r) => {
          this.userService.findUser(r.user.email).subscribe((result) => {
            // @ts-ignore
            if (result.length == 0)
              this.userService
                .createuser(
                  r.user.uid,
                  new Profile(
                    0,
                    '',
                    '',
                    r.user.email,
                    ['Guatemala'],
                    0,
                    r.user.displayName,
                    '',
                    0,
                    r.user.displayName,
                    String(new Date()),
                    String(new Date()),
                    ''
                  )
                )
                .then((result) => {
                  r.user
                    .sendEmailVerification()
                    .then((fin) => {
                      resolve(
                        'Te hemos enviado un correo para que verifiques tu cuenta.'
                      );
                    })
                    .catch((error2) => {
                      reject(error2.message);
                    });
                })
                .catch((error1) => {
                  reject(error1.message);
                });
            else {
              sessionStorage.setItem('User', this.set(r.user.uid));
              sessionStorage.setItem('Admin', this.set('false'));
              //sessionStorage.setItem('UserInfo', JSON.stringify(r.user));
              resolve('Bienvenido, ' + r.user.displayName);
            }
          });
        })
        .catch((error) => {
          reject(error.message);
        });
    });
  }

  deleteuser() {
    this.afAuth.authState.subscribe((state) => {
      state
        .delete()
        .then((_) => console.log('deleted!'))
        .catch((e) => console.log(e));
    });
  }
}
