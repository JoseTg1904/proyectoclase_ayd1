import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/firestore";
import {AngularFireStorage} from "@angular/fire/storage";

@Injectable({
  providedIn: 'root'
})
export class FirebaseProductServiceService {

  constructor(private FirebaseService: AngularFirestore, private storage: AngularFireStorage) { }

  public referenciaCloudStorageProducts(nombreArchivo: string) {
    return this.storage.ref('productos/'+nombreArchivo);
  }

  public referenciaCloudStorageUser(nombreArchivo: string) {
    return this.storage.ref('usuarios/'+nombreArchivo);
  }

  public tareaCloudStorageProducts(nombreArchivo: string, datos: any) {
    return this.storage.upload('productos/'+nombreArchivo, datos);
  }

  public tareaCloudStorageUsers(nombreArchivo: string, datos: any) {
    return this.storage.upload('usuarios/'+nombreArchivo, datos);
  }

  readProduct(uid) {
    return this.FirebaseService.collection('catalogo').doc(uid).snapshotChanges();
  }

  readProductsrc(data)
  {
    return this.FirebaseService.collection('catalogo', ref => ref.where('name', '==', data)).snapshotChanges();
  }
  readProducts() {
    return this.FirebaseService.collection('catalogo').snapshotChanges();
  }

  readCategories() {
    return this.FirebaseService.collection('categoria').snapshotChanges();
  }

  obtenerOrdenes(){
    return this.FirebaseService.collection('orden').snapshotChanges();
  }
}
