export class User {
  constructor(
    public username: string,
    public email: string,
    public pwd: string
  ) { }
}
export class Profile {
  constructor(
    public edad: number,
    public nit: string,
    public apellido: string,
    public correo: string,
    public direccion: any[],
    public dpi: number,
    public nombre: string,
    public profile: string,
    public telefono: number,
    public usuario: string,
    public creacion: string,
    public modificacion: string,
    public picture: string
  ){}

}

export interface notification
{
  severity:string;
  summary: string;
  detail: string;
}
