export class Products {
  constructor(
    public nombre: string,
    public decripcion: string,
    public picture: string,
    public precio: number,
    public categoria: string,
    public detalles: [],
    public inventario: number
  ){
    if(detalles==undefined)
    {
      this.detalles = [];
    }
  }
}
