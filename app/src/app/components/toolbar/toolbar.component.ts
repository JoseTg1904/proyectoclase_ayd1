import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { AuthUserService } from 'src/app/shared/services/auth-user.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css'],
})
export class ToolbarComponent implements OnInit {
  @Output() toggle = new EventEmitter<void>();
  route: Boolean;
  hide = true;
  isAdmin = false;

  constructor(private router: Router, private authService: AuthUserService) {
    this.route = true;
  }

  ngOnInit(): void {
    this.isAdmin = this.authService.adminActive;
    let urls = this.router.url;
    if (urls.indexOf('Profile') !== -1) this.route = false;
  }

  toggleEmit(): void {
    this.toggle.emit();
  }

  hideSearch(): void {
    this.hide = !this.hide;
    console.log(this.hide);
  }

  logout(): void {
    sessionStorage.removeItem('Admin');
    sessionStorage.removeItem('User');
    localStorage.removeItem('cart');
    if (!this.isAdmin) this.authService.Logout();
    else this.authService.LogoutAdmin();
    this.router.navigate(['']);
  }
}
