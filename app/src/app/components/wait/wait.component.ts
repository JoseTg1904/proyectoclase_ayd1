import { Component, OnInit } from '@angular/core';
import {AuthUserService} from "../../shared/services/auth-user.service";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Component({
  selector: 'app-wait',
  templateUrl: './wait.component.html',
  styleUrls: ['./wait.component.css']
})
export class WaitComponent implements OnInit {

  constructor(
    private afAuth: AuthUserService,
    private _snackBar: MatSnackBar,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.afAuth.ResultRedirect().then(
      (value) =>
      {
        if(value!='noyet')
        {
          //@ts-ignore
          if(value != '') this.openSnackBar(value);
          else this.openSnackBar('Bienvenido');
          this.router.navigate(['']);
        }
      },
      (error) => {
        this.openSnackBar(error);
        this.router.navigate(['/Login']);
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'Aceptar', {
      duration: 5000,
    });
  }

}
