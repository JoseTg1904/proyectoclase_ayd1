import {Component, OnInit, EventEmitter, Input, Output, OnChanges, SimpleChanges} from '@angular/core';
import { ChartOptions, ChartType } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';
import {ProductoService} from "../../../shared/services/producto.service";
import {DataCategorias} from "../../../pages/dash/dash.component";

function getRandomColor() {
  var color = Math.floor(0x1000000 * Math.random()).toString(16);
  return '#' + ('000000' + color).slice(-6);
}

@Component({
  selector: 'app-categoria-producto',
  templateUrl: './categoria-producto.component.html',
  styleUrls: ['./categoria-producto.component.css']
})
export class CategoriaProductoComponent implements OnInit, OnChanges  {


  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: { position: "right"}
  };
  public pieChartLabels: Label[] = [];
  public pieChartData: SingleDataSet = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public infoCategoria = [];
  Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];

  public pieChartColors = [
    {
      backgroundColor: [],
    },
  ];

  constructor(private productoservice: ProductoService) {

  }

  ngOnInit() {
    this.pieChartLabels = [];
    this.pieChartData = [];
    this.productoservice.obtenerCategorias().subscribe({
      next: categoria => {
        try {
          categoria.forEach(value => {
            // @ts-ignore
            let data = value.payload.doc.data();
            let cate: DataCategorias =
              {
                id: '/categoria/' + value.payload.doc.id,
                nombre: data["nombre"],
                cantidad: 0,
                color: getRandomColor(),
                mes: "",
                anio: ""
              }
            let productosCat = [];
            this.productoservice.getProductCategory(cate.id).subscribe(
              {
                next: refs =>
                {
                  refs.forEach((refsP)=>{
                    productosCat.push(refsP.payload.doc.id);
                  })
                  this.productoservice.obtenerOrdenes().subscribe(
                    {
                      next: refsO =>
                      {
                        refsO.forEach((refsOrden)=>
                        {
                          let ordenP = refsOrden.payload.doc.data();
                          let ProductosinOrden = ordenP["productos"];
                          let fec = ordenP["status"]
                          if(fec != 'Cancelado')
                          {
                            ProductosinOrden.forEach((productoinProductos)=>
                            {
                              let productito = JSON.parse(productoinProductos);
                              if(productosCat.includes(productito.producto)) cate.cantidad += productito.cantidad;
                            })
                          }
                        });
                        if(cate.cantidad>0)
                        {
                          if(!this.pieChartLabels.includes(cate.nombre))
                          {
                            this.pieChartData.push(cate.cantidad);
                            this.pieChartLabels.push(cate.nombre);
                            this.pieChartColors[0].backgroundColor.push(cate.color);
                          }
                        }
                      }
                    }
                  )
                }
              }
            );
          });
        } catch (e) {
          console.log(e);
        }
      }
    });
  }

  public chartClicked(e:any):void {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if ( activePoints.length > 0) {
        const clickedElementIndex = activePoints[0]._index;
        //this.dataSendFromChildCategoria.emit(chart.data.labels[clickedElementIndex]);
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.pieChartLabels = [];
    this.pieChartData = [];
    this.productoservice.obtenerCategorias().subscribe({
      next: categoria => {
        try {
          categoria.forEach(value => {
            // @ts-ignore
            let data = value.payload.doc.data();
            let cate: DataCategorias =
              {
                id: '/categoria/' + value.payload.doc.id,
                nombre: data["nombre"],
                cantidad: 0,
                color: getRandomColor(),
                mes: "",
                anio: ""
              }
            let productosCat = [];
            this.productoservice.getProductCategory(cate.id).subscribe(
              {
                next: refs =>
                {
                  refs.forEach((refsP)=>{
                    productosCat.push(refsP.payload.doc.id);
                  })
                  this.productoservice.obtenerOrdenes().subscribe(
                    {
                      next: refsO =>
                      {
                        refsO.forEach((refsOrden)=>
                        {
                          let ordenP = refsOrden.payload.doc.data();
                          let ProductosinOrden = ordenP["productos"];
                          let fec = ordenP["status"]
                          if(fec != 'Cancelado')
                          {
                            ProductosinOrden.forEach((productoinProductos)=>
                            {
                              let productito = JSON.parse(productoinProductos);
                              if(productosCat.includes(productito.producto)) cate.cantidad += productito.cantidad;
                            })
                          }
                        });
                        if(cate.cantidad>0)
                        {
                          if(!this.pieChartLabels.includes(cate.nombre))
                          {
                            this.pieChartData.push(cate.cantidad);
                            this.pieChartLabels.push(cate.nombre);
                            this.pieChartColors[0].backgroundColor.push(cate.color);
                          }
                        }
                      }
                    }
                  )
                }
              }
            );
          });
        } catch (e) {
          console.log(e);
        }
      }
    });
  }

  /*
  ngOnChanges(changes: SimpleChanges): void {
    if(!changes.categoriaFind.firstChange)
    {
      this.pieChartLabels = [];
      this.pieChartData = [];
      this.productoservice.obtenerCategorias().subscribe({
        next: categoria => {
          try {
            categoria.forEach(value => {
              // @ts-ignore
              let data = value.payload.doc.data();
              let cate: DataCategorias =
                {
                  id: '/categoria/' + value.payload.doc.id,
                  nombre: data["nombre"],
                  cantidad: 0,
                  color: getRandomColor(),
                  mes: "",
                  anio: ""
                }
              let productosCat = [];
              this.productoservice.getProductCategory(cate.id).subscribe(
                {
                  next: refs =>
                  {
                    refs.forEach((refsP)=>{
                      productosCat.push(refsP.payload.doc.id);
                    })
                    this.productoservice.obtenerOrdenes().subscribe(
                      {
                        next: refsO =>
                        {
                          refsO.forEach((refsOrden)=>
                          {
                            let ordenP = refsOrden.payload.doc.data();
                            let ProductosinOrden = ordenP["productos"];
                            ProductosinOrden.forEach((productoinProductos)=>
                            {
                              let productito = JSON.parse(productoinProductos);
                              if(productosCat.includes(productito.producto)) cate.cantidad += productito.cantidad;
                            })

                          });
                          if(cate.cantidad>0)
                          {
                            if(this.categoriaFind == cate.nombre || this.categoriaFind == "")
                            {
                              this.pieChartData.push(cate.cantidad);
                              this.pieChartLabels.push(cate.nombre);
                              this.pieChartColors[0].backgroundColor.push(cate.color);
                            }
                          }
                        }
                      }
                    )
                  }
                }
              );
            });
          } catch (e) {
            console.log(e);
          }
        }
      });
    }

  }
*/
}
