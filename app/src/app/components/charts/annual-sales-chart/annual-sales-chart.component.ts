import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
import {ChartDataSets, ChartOptions, ChartType} from 'chart.js';
import {Color, Label} from 'ng2-charts';
import {ProductoService} from "../../../shared/services/producto.service";
import {DataCategorias} from "../../../pages/dash/dash.component";

export interface Fechas {
  Mes: string;
  Anio: number;
  Cantidad: number;
}

@Component({
  selector: 'app-annual-sales-chart',
  templateUrl: './annual-sales-chart.component.html',
  styleUrls: ['./annual-sales-chart.component.css']
})
export class AnnualSalesChartComponent implements OnInit, OnChanges {


  public lineChartData: ChartDataSets[] = [
    {data: [], label: 'Ventas', fill: false, lineTension: 0},
  ];

  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    legend: {position: "right"}
  };
  public lineChartColors: Color[] = [
    {
      borderColor: 'orange',
      backgroundColor: 'transparent',
      borderDash: [5, 5],
      pointBorderColor: 'orange',
      pointBackgroundColor: 'rgba(255,150,0,0.5)',
      pointRadius: 5,
      pointHoverRadius: 10,
      pointHitRadius: 30,
      pointBorderWidth: 2,
      pointStyle: 'rectRounded'
    },
  ];
  public lineChartLegend = true;
  public lineChartType: ChartType = 'line';
  public lineChartPlugins = [];
  public categos;

  constructor(private productoservice: ProductoService) { }


  ngOnInit() {
    //Generando Fechas -
    this.lineChartData[0].data = [];
    this.lineChartLabels = [];
    let Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    let fechaActual = new Date();
    let AnioActual = fechaActual.getFullYear();
    //obtener los ultimos 6 meses
    for(let a = 5; a>-1; a--)
    {

      let NumMes = ((fechaActual.getMonth()-a)<0)? (12+(fechaActual.getMonth()-a)) : fechaActual.getMonth()-a;
      let AnioCurrent = AnioActual - ((fechaActual.getMonth()-a<0)?1:0);
      let Fecha: Fechas = {
        Mes: Meses[NumMes],
        Anio: AnioCurrent,
        Cantidad: 0
      }
      this.productoservice.obtenerOrdenes().subscribe({
        next: ordenes=>{
          ordenes.forEach(og=>
          {
            let data = og.payload.doc.data();
            let fec = data["fecha"]
            let find = new Date(fec);
            var change = false;
            if(NumMes == find.getMonth() && AnioCurrent == find.getFullYear() && data["status"] != "Cancelado") Fecha.Cantidad++;
          });
          if(!this.lineChartLabels.includes(Fecha.Mes))
          {
            this.lineChartData[0].data.push(Fecha.Cantidad);
            this.lineChartLabels.push(Fecha.Mes);
          }

        }
      });
    }
  }

  public chartClicked(e:any):void {
    if (e.active.length > 0) {
      const chart = e.active[0]._chart;
      const activePoints = chart.getElementAtEvent(e.event);
      if ( activePoints.length > 0) {
        const clickedElementIndex = activePoints[0]._index;
        let Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
        //this.dataSendFromChildCategoria.emit(Meses.indexOf(chart.data.labels[clickedElementIndex]));
      }
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    //Generando Fechas -
    this.lineChartData[0].data = [];
    this.lineChartLabels = [];
    let Meses = ['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
    let fechaActual = new Date();
    let AnioActual = fechaActual.getFullYear();
    //obtener los ultimos 6 meses
    for(let a = 5; a>-1; a--)
    {

      let NumMes = ((fechaActual.getMonth()-a)<0)? (12+(fechaActual.getMonth()-a)) : fechaActual.getMonth()-a;
      let AnioCurrent = AnioActual - ((fechaActual.getMonth()-a<0)?1:0);
      let Fecha: Fechas = {
        Mes: Meses[NumMes],
        Anio: AnioCurrent,
        Cantidad: 0
      }
      this.productoservice.obtenerOrdenes().subscribe({
        next: ordenes=>{
          ordenes.forEach(og=>
          {
            let data = og.payload.doc.data();
            let fec = data["fecha"]
            let find = new Date(fec);
            var change = false;
            if(NumMes == find.getMonth() && AnioCurrent == find.getFullYear() && data["status"] != "Cancelado") Fecha.Cantidad++;
          });
          if(!this.lineChartLabels.includes(Fecha.Mes))
          {
            this.lineChartData[0].data.push(Fecha.Cantidad);
            this.lineChartLabels.push(Fecha.Mes);
          }

        }
      });
    }
  }

}
