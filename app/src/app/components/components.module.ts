import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { DrawerComponent } from './drawer/drawer.component';
import {ToastsContainer} from "./toasts-container.component";
import {NgbToastModule} from "@ng-bootstrap/ng-bootstrap";
import {MaterialModuleModule} from "../modules/material-module/material-module.module";
import { ToolbarAdminComponent } from './toolbar-admin/toolbar-admin.component';
import { WaitComponent } from './wait/wait.component';
import { ChartsModule} from "ng2-charts";
import { CategoriaProductoComponent } from './charts/categoria-producto/categoria-producto.component';
import { AnnualSalesChartComponent } from './charts/annual-sales-chart/annual-sales-chart.component';

@NgModule({
  declarations: [ToolbarComponent, DrawerComponent, ToastsContainer, ToolbarAdminComponent, WaitComponent, CategoriaProductoComponent, AnnualSalesChartComponent],
    imports: [CommonModule, NgbToastModule, MaterialModuleModule, ChartsModule],
  exports: [ToolbarComponent, DrawerComponent, ToastsContainer, ToolbarAdminComponent, CategoriaProductoComponent, AnnualSalesChartComponent],
})
export class ComponentsModule {}
