import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AuthUserService} from "../../shared/services/auth-user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-toolbar-admin',
  templateUrl: './toolbar-admin.component.html',
  styleUrls: ['./toolbar-admin.component.css']
})
export class ToolbarAdminComponent implements OnInit {

  @Output() toggle = new EventEmitter<void>();
  @Input() user:string;

  constructor(private adauth: AuthUserService,
              private router: Router) { }

  ngOnInit(): void {
  }

  toggleEmit(): void {
    this.toggle.emit();
  }

  LogoutAdmin() {
    this.adauth.LogoutAdmin();
    this.router.navigate(['']);
  }
}
