import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { menuDrawer } from 'src/app/shared/models/menuDrawer';

@Component({
  selector: 'app-drawer',
  templateUrl: './drawer.component.html',
  styleUrls: ['./drawer.component.css'],
})
export class DrawerComponent implements OnInit {
  @Input() menuList: menuDrawer[];
  @Output() drawerEvent = new EventEmitter<string>();

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );
  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {}

  ngOnInit(): void {}

  drawerEmit(path: string) {
    this.drawerEvent.emit(path);
  }

  viewProfile(): void {
    this.router.navigateByUrl('home/Profile');
  }

  home(): void {
    this.router.navigateByUrl('home');
  }
}
