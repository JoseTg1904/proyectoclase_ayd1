// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDOVwFGsfS1cL38ioJYWlGAzkHqERDho2Q",
    authDomain: "analisis1-usac.firebaseapp.com",
    projectId: "analisis1-usac",
    storageBucket: "analisis1-usac.appspot.com",
    messagingSenderId: "1063907619085",
    appId: "1:1063907619085:web:e13b9d895ecfe05e9f4044",
    measurementId: "G-2ZH38845G5"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
