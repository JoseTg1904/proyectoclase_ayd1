# ProyectoClase_AD1

## Iconos

```html
<!-- menu hamburguesa -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-menu"
></span>
<!-- flecha arriba -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-up-arrow-alt"
></span>
<!-- carrito -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-cart"
></span>
<!-- buscador -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-search-alt"
></span>
<!-- grid -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-grid-small"
></span>
<!-- lista -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-list-ul"
></span>
<!-- estrella -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-star"
></span>
<!-- cerrar -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-x"
></span>
<!-- usuario -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-user"
></span>
<!-- llave -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-key"
></span>
<!-- mostrar -->
<span
  class="iconify"
  data-inline="false"
  data-icon="bx:bx-show"
></span>
<!-- google -->
<span
    class="iconify"
    data-icon="flat-color-icons:google"
    data-inline="false"
></span>
```

## Controles

### Formulario

```html
<div class="form__container">
  <div class="input__group"></div>
  <button class="button__flatter--accent">Action</button>
</div>
```

### Inputs

```html
<div class="input__group">
  <input type="text" placeholder="input" required />
</div>
```

### Select

```html
<div class="input__group">
  <p>Titulo</p>
    <select name="select" id="select">
      <option value="Value 1">Option 1</option>
    <option value="Value 2">Option 2</option>
    </select>
</div>
```

### Input radio y checkbox

- Grupo de input radio y checkbox

```html
<div class="input__group flex-column">
  <div class="input__multigroup">
    <input type="radio" name="radio" id="radio1" />
    <label for="radio1">radio</label>
  </div>
  <div class="input__multigroup">
    <input type="radio" name="radio" id="radio2" />
    <label for="radio2">radio</label>
  </div>
  <div class="input__multigroup">
    <input type="radio" name="radio" id="radio3" />
    <label for="radio3">radio</label>
  </div>
</div>
```

- Input radio y checkbox simple

```html
<div class="input__group input__group--simple">
  <input type="checkbox" name="checkbox" id="checkbox0" />
  <label for="checkbox0">checkbox0</label>
</div>
```

### Botones

```html
<!-- Botón plano con color de fondo -->
<button class="button__flatter--primary">Action</button>
<button class="button__flatter--accent">Action</button>
<!-- Botón tipo enlace con color de fondo -->
<button class="button__linked--primary">Action</button>
<button class="button__linked--accent">Action</button>

```

### Tipos de texto

```html
<!-- Texto para descripciones -->
<p class="text__description--primary">Parrafo</p>
<p class="text__description--accent">Parrafo</p>
<h1 class="text__description--secondary">Parrafo</h1>
<!-- Texto fuerte para mostrar información -->
<h2 class="text__info--primary">Parrafo</h2>
<h3 class="text__info--accent">Parrafo</h3>
```

## Toolbar

La barra toolbar es un componente llamado `app-toolbar`. Implementar el método `toolbar` para recibir la acción del botón de hamburguesa. Así mismo implementar en el método que se dispara, la negación de una variable booleana para controlar el toggle del drawer.

```html
<app-toolbar class="toolbar" (toggle)="toggleListen()"></app-toolbar>
```

```ts
toggleDrawer: boolean = false;

toggleListen() {
  this.toggleDrawer = !this.toggleDrawer;
}
```

Para esto, en el componente drawer habilitar switch de la clase `drawer--hide`

```html
<app-drawer
  class="drawer"
  [ngClass]="toggleDrawer ? '' : 'drawer--hide'"
  ></app-drawer>
```

## Página principal

La página principal se compone por un conjunto de componentes que se conforman de la siguiente manera:

```html
<app-toolbar class="toolbar"></app-toolbar>
<div class="body">
    <app-drawer class="drawer"></app-drawer>
    <router-outlet></router-outlet>
</div>
```

## Contenido

### Grilla inicio

La grilla para mostrar los productos de la pagina principal. El contenedor será un `home__container` y cada producto un `home__item`

```html
<div class="home__container">
    <div class="home__item" (click)="">
        <div class="item__image">
            <img 
                src="https://loremflickr.com/640/360" 
                alt="Product image"
            />
        </div>
        <div class="item__description">
            <p class="item__price text__descripcion--secondary">
                Price1
            </p>
            <h2 class="item__name text__info--primary">
                Name 1
            </h2>
        </div>
    </div>
</div>
```

### Grilla tienda

La grilla para mostrar los productos de la pagina de la tienda. El contenedor será un `store__container` y cada producto un `store__item`.

```html
<div class="store__container">
    <div class="store__item">
        <div class="item__image">
        <img 
            src="https://loremflickr.com/640/360"
            alt="Product image"
        />
        </div>
        <div class="item__footer">
        <div class="item__description" (click)="">
            <p class="item__price text__descripcion--secondary">
                Price 1
            </p>
            <h2 class="item__name text__info--primary">
                Name 1
            </h2>
        </div>
        <div class="item__action" (click)="">
            <span
            class="iconify"
            data-inline="false"
            data-icon="bx:bx-cart">
            </span>
        </div>
        </div>
    </div>
</div>
```

### Detalle del producto

```html
<div class="product__container">
    <div class="product__item">
        <div class="product__header">
        <div class="header__image">
            <img src="https://loremflickr.com/640/360" alt="product_image" />
            <div class="header__title">
            <h3 class="text__descripcion--accent">price</h3>
            <h1 class="text__info--primary">name</h1>
            <p class="text__descripcion--secondary">detail product</p>
            </div>
        </div>
        </div>
        <div class="product__description">
        <p>
            Lorem, ipsum dolor sit amet consectetur adipisicing elit. Optio at
            asperiores non repudiandae cumque eaque facere quos, repellendus nisi
            reprehenderit dolor, animi unde esse neque architecto sunt, dolores
            vitae fugit.
        </p>
        </div>
        <div class="product__action">
        <div class="input__group">
            <p>Quantity</p>
            <select name="select" id="select">
            <option value="Value 1">Option 1</option>
            <option value="Value 2">Option 2</option>
            <option value="Value 3">Option 3</option>
            <option value="Value 4">Option 4</option>
            <option value="Value 5">Option 5</option>
            </select>
        </div>
        <button class="button__flatter--accent">Add to cart</button>
        </div>
    </div>
</div>
```

### Productos en el carrito

Para el producto en el carrito, se debe tener un contenedor y dentro el título y la tabla de productos como se define debajo

```html
  <div class="list__container">
    <h1 class="text__info--primary">Shopping Cart</h1>
    <table class="table">
      <tr class="header">
        <th>Photo</th>
        <th>Name</th>
        <th>Price</th>
        <th>Quantity</th>
      </tr>
      <tr class="body">
        <td>
          <img src="http://loremflickr.com/640/360" alt="product_photo" />
        </td>
        <td>Producto 2</td>
        <td><span>Q </span> 130</td>
        <td>4</td>
      </tr>
    </table>
  </div>
```

### Total a pagar

Para el card de total a pagar, debe seguier la plantilla de abajo

```html
    <div class="card__total">
      <div class="card__header">
        <h2>Card Total</h2>
      </div>
      <div class="card__item">
        <p class="text__description--secondary">Subtotal</p>
        <p class="text__description--secondary">Q 140.00</p>
      </div>
      <div class="card__item">
        <p class="text__description--secondary">Delivery</p>
        <p class="text__description--secondary">Free</p>
      </div>
      <div class="card__item">
        <p class="text__description--secondary">Total</p>
        <p class="text__description--secondary">Q 140.00</p>
      </div>
      <div class="card__action">
        <button class="button__flatter--accent">Checkout</button>
      </div>
    </div>
```
